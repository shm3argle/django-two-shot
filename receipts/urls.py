from django.urls import path
from . import views
from .views import receipt_create_view, category_list_view, account_list_view, create_category, create_account

urlpatterns = [
    path("", views.receipt_list_view, name="home"),
    path("create/", receipt_create_view, name="create_receipt"),
    path("categories/", category_list_view, name="category_list"),
    path("accounts/", account_list_view, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
