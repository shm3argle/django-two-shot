from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt

# Register your models here.


class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "owner")


class AccountAdmin(admin.ModelAdmin):
    list_display = ("name", "number", "owner")


class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )


admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Receipt, ReceiptAdmin)
